package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long orderId;

    private Double total;
    private Date createdDate;
    private Integer status;

    @OneToOne(mappedBy = "order")
    private Comment comment;

    @ManyToOne
    @JoinColumn(name = "accountId")
    private Account account;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<DishOrder> dishOrders = new HashSet<>();
}
