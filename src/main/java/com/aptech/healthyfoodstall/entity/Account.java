package com.aptech.healthyfoodstall.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long accountId;

    private String username;
    private String password;
    private String email;
    private String phone;
    private String address;
    private Date birthdate;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private Set<Order> orders = new HashSet<>();

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private Set<Comment> comments = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true, name = "userDetailId")
    private UserDetail userDetail;

    @ManyToOne
    @JoinColumn(name = "roleId")
    private Role role;
}
