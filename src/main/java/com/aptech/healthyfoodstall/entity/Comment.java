package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long commentId;

    private String description;
    private Float rating;

    @OneToMany(mappedBy = "comment", cascade = CascadeType.ALL)
    private Set<ImageInComment> imageInComments = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "accountId")
    private Account account;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true, name = "orderId")
    private Order order;
}
