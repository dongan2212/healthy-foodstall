package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;

@Entity
public class MealDishFood {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "mealId")
    private Meal meal;

    @ManyToOne
    @JoinColumn(name = "dishId")
    private Dish dish;

    @ManyToOne
    @JoinColumn(name = "foodId")
    private Food food;
}
