package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;

@Entity
public class UserDetail {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long userDetailId;

    private Double weight;
    private Double height;
    private Integer sportFrequency;
    private Integer weightTrend;

    @OneToOne(mappedBy = "userDetail")
    private Account account;
}
