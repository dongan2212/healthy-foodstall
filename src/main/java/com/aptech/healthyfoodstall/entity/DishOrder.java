package com.aptech.healthyfoodstall.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class DishOrder implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "dishId")
    private Dish dish;

    @Id
    @ManyToOne
    @JoinColumn(name = "orderId")
    private Order order;

    private Integer quantity;
    private Double total;
}
