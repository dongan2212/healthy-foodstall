package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;

@Entity
public class ImageInComment {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long imageId;

    private String imageUrl;

    @ManyToOne
    @JoinColumn(name = "commentId")
    private Comment comment;
}
