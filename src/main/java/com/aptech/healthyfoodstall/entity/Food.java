package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Food {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long foodId;

    private String name;
    private String imageUrl;
    private Double ingredient;

    @OneToMany(mappedBy = "food", cascade = CascadeType.ALL)
    private Set<MealDishFood> mealDishFoods;

}
