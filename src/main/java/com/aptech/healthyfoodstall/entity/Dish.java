package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Dish {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long dishId;

    private String name;
    private String imageUrl;
    private Double price;

    @OneToMany(mappedBy = "dish", cascade = CascadeType.ALL)
    private Set<DishOrder> dishOrders = new HashSet<>();

    @OneToMany(mappedBy = "dish", cascade = CascadeType.ALL)
    private Set<MealDishFood> mealDishFoods = new HashSet<>();
}
