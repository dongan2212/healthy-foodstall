package com.aptech.healthyfoodstall.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Meal {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long mealId;

    private String mealName;

    @OneToMany(mappedBy = "meal", cascade = CascadeType.ALL)
    private Set<MealDishFood> mealDishFoods;
}
