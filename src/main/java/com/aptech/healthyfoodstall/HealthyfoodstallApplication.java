package com.aptech.healthyfoodstall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthyfoodstallApplication {

    public static void main(String[] args) {
        SpringApplication.run(HealthyfoodstallApplication.class, args);
    }

}
